<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_transaksi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_barang');
            $table->bigInteger('id_platform');
            $table->bigInteger('id_pembeli');
            $table->string('jumlah');
            $table->string('jenis_pembayaran');
            $table->string('status_pembayaran');
            $table->string('tanggal_order');
            $table->string('tanggal_bayar');
            $table->string('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_transaksi');
    }
}
