<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    public $table = "table_barang";
    protected $fillable = [
        'kode_barang', 'nama_barang', 'distributor', 'stock', 'harga_beli', 'harga_jual', 'profit', 'author', 'updater'
    ];
}
