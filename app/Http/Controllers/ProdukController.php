<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;

class ProdukController extends Controller
{
    public function index()
    {
        $produk = Produk::select('kode_barang')->orderBy('kode_barang', 'DESC')->first();
        if(!$produk) {
            $kode = "PRD0001";
        } else {
            $kode = generateCode($produk);
        }
        return view('produk.tambah', ["kode_produk"=>$kode]);
    }

    public function generateCode($kode)
    {
        $kode_split = substr($kode, 3);
        $kode_init = substr($kode, 0, -4);
        return $kode_split.str_pad($kode_init+1, 4, "0", STR_PAD_LEFT);
    }

    public function daftar(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'manager']);
        return view('produk.daftar');
    }
}
