@extends('app.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tambah Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Produk</a></li>
            <li class="active">Tambah</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <form role="form">
                    <div class="col-md-6" style="padding: 0px !important">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Produk Baru</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">KODE PRODUK</label>
                                    <input type="text" class="form-control" value="{{$kode_produk}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Produk</label>
                                    <input type="text" class="form-control" placeholder="ex. Jam Tangan" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Distributor Produk</label>
                                    <input type="text" class="form-control" placeholder="ex. Pasar Senen" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Stock Produk</label>
                                    <input type="number" class="form-control" placeholder="ex. 8" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Beli</label>
                                    <input type="number" class="form-control" id="harga-b" placeholder="ex. 20000 (Hanya angka)" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Harga Jual</label>
                                    <input type="number" class="form-control" id="harga-j" placeholder="ex. 20000 (Hanya angka)" required>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Profit</label>
                                    <input type="number" class="form-control" id="profit" placeholder="ex. 20000 (Hanya angka)" readonly>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                    <div class="col-md-6" style="padding-right: 0px !important">
                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Gambar Produk</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputFile">File input</label>
                                    <input type="file" id="exampleInputFile">
                                </div>
                                <div class="form-group">
                                    <img src="" id="img-prev" class="img-fluid" style="max-height: 430px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                </form>
            </div>
                <!-- /.box -->
        </div>
    </section>
</div>
@endsection
@section('js-here')
<script>
    $('#harga-j').on('change', function(){
        var harga_beli = $('#harga-b').val()
        var harga_jual = $('#harga-j').val()
        var profit = harga_jual - harga_beli;
        $('#profit').val(profit);
    });
    $('#harga-b').on('change', function(){
        var harga_beli = $('#harga-b').val()
        var harga_jual = $('#harga-j').val()
        var profit = harga_jual - harga_beli;
        $('#profit').val(profit);
    });
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
        $('#img-prev').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
    }

    $("#exampleInputFile").change(function() {
        readURL(this);
    });
</script>
@endsection