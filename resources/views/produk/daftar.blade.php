@extends('app.master')
@section('ex-css')
<style>
    .card {
        background: #fff;
    }
</style>
@endsection
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Daftar Produk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Produk</a></li>
            <li class="active">Daftar</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-3 col-sm-3">
                <div class="box box-primary">
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/photo2.png" class="img-responsive pad" alt="...">
                    <div class="box-body">
                        <h5 class="box-title">Card title</h5>
                        <p class="box-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="box box-primary">
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/photo2.png" class="img-responsive pad" alt="...">
                    <div class="box-body">
                        <h5 class="box-title">Card title</h5>
                        <p class="box-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="box box-primary">
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/photo2.png" class="img-responsive pad" alt="...">
                    <div class="box-body">
                        <h5 class="box-title">Card title</h5>
                        <p class="box-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="box box-primary">
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/photo2.png" class="img-responsive pad" alt="...">
                    <div class="box-body">
                        <h5 class="box-title">Card title</h5>
                        <p class="box-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <div class="box box-primary">
                    <img src="https://adminlte.io/themes/AdminLTE/dist/img/photo2.png" class="img-responsive pad" alt="...">
                    <div class="box-body">
                        <h5 class="box-title">Card title</h5>
                        <p class="box-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@section('js-here')

@endsection