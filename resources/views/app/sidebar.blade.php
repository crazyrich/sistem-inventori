<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-files-o"></i>
                <span>Transaksi</span>
                <span class="pull-right-container">
                    <span class="label label-danger pull-right">4</span>
                </span>
                </a>
                <ul class="treeview-menu">
                <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Transaksi Masuk</a></li>
                <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Transaksi Keluar</a></li>
                <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Daftar Transaksi</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Produk</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('produk/tambah') }}"><i class="fa fa-plus"></i> Tambah Produk</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Daftar Produk</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-laptop"></i>
                    <span>Platform</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/UI/general.html"><i class="fa fa-plus"></i> Tambah Platform</a></li>
                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Daftar Platform</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                <i class="fa fa-pie-chart"></i>
                <span>Kategori</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('produk/tambah') }}"><i class="fa fa-plus"></i> Tambah Kategori</a></li>
                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Daftar Kategori</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
    </aside>